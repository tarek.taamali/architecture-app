# Inetum Architecture


## Explication de l'architecture

Architecture de l'application 

```
lib/
|- app/
|- main.dart
```

```
app/
|- common/
   |- values/
   |- utils/
   |- theme/
   |- constants.dart
|- common_widgets/
|- data/
   |- models/
   |- provider/
   |- services/
|- modules/
   |- home/
   |- details/
|- routes/
```

 Maintenant , on va voir la dossier lib qui contient code de l'application 
```
1- common_widget - contient les widgets qui ont réutiliser  par les modules ***Modules**  
1- common - Contient tous les utilitaires ou fonctions communes qui sont utilisés dans toute l'application. le dossier contient  `constants`. `utils`, `theme`, `strings`, `dimensions`, `colors`.
2- data - contient la couche de données de l'application .
3- modules - Contient tous les screens de l'application .
4- routes — Contient les fichiers des routes de l'application.
5- main.dart - C'est le point d'entrée de l'application.

## Ui de l'application
 
 ![](/screenshots/image1.jpg)

 ![](/screenshots/image2.jpg)

