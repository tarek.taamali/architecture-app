
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:inetum/app/data/provider/api.dart';
import 'package:inetum/app/data/services/post_repository.dart';
import 'package:inetum/app/modules/user_details/controllers/user_details_controller.dart';

class UserDetailsBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UserDetailsController>(() {
      return UserDetailsController(
          PostRepository(apiClient: ApiClient(httpClient: http.Client())));
    });
 
  }
}

