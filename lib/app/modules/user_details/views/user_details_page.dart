import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:inetum/app/common_widget/loading_widget.dart';
import 'package:inetum/app/modules/user_details/controllers/user_details_controller.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'component/post_widget.dart';
import 'component/user_details_widget.dart';

class UserDetailsPage extends GetView<UserDetailsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
      ),
      body: SafeArea(
        child: Obx(
          () => Column(children: [
            Expanded(
              flex: 1,
              child: Container(
                  //  color: Colors.red,
                  child: UserDetailsWidget(
                user: controller.userDetail,
              )),
            ),
            Divider(
              height: 10,
            ),
            Expanded(
                flex: 3,
                child: LazyLoadScrollView(
                  onEndOfPage: controller.loadNextPage,
                  isLoading: controller.lastPage,
                  child: controller.posts.length == 0
                      ? Center(child: LoadingWidget())
                      : ListView.builder(
                          itemCount: controller.posts.length,
                          itemBuilder: (context, index) {
                            final _post = controller.posts[index];
                            return PostWidget(
                                post: _post,
                                isLast: controller.posts.length - 1 == index);
                          },
                        ),
                )),
          ]),
        ),
      ),
    );
  }
}
