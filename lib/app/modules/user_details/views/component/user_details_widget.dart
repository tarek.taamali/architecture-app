import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:iconly/iconly.dart';
import 'package:inetum/app/common/values/app_colors.dart';
import 'package:inetum/app/common_widget/circle_avatar_widget.dart';
import 'package:inetum/app/data/model/user.dart';

class UserDetailsWidget extends StatelessWidget {
  final User user;

  const UserDetailsWidget({Key key, @required this.user}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        //  padding: EdgeInsets.all(32),
        child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 25,
        ),
        Container(
          width: double.infinity,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                  child: Container(
                      child: CircleAvatarWidget(
                avatarUrl: user.avatarUrl,
                height: 120,
                width: 120,
              ))),
              Expanded(
                flex: 2,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("${user.name}", style: TextStyle(fontSize: 25)),
                      SizedBox(height: 10),
                      RichText(
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(IconlyBroken.profile),
                            ),
                            WidgetSpan(
                              child: SizedBox(
                                width: 5,
                              ),
                            ),
                            TextSpan(
                                text: "${user.userName}",
                                style: TextStyle(color: AppColors.kBlackColor)),
                          ],
                        ),
                      ),
                      SizedBox(height: 10),
                      RichText(
                        text: TextSpan(
                          children: [
                            WidgetSpan(
                              child: Icon(IconlyBroken.message),
                            ),
                            WidgetSpan(
                              child: SizedBox(
                                width: 5,
                              ),
                            ),
                            TextSpan(
                                text: "${user.email}",
                                style: TextStyle(color: AppColors.kBlackColor)),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        )
      ],
    ));
  }
}
