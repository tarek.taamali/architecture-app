import 'package:flutter/material.dart';
import 'package:inetum/app/common/utils/util.dart';
import 'package:inetum/app/common/values/app_colors.dart';
import 'package:inetum/app/common_widget/error_loading_image_widget.dart';
import 'package:inetum/app/data/model/post.dart';

class PostWidget extends StatelessWidget {
  final Post post;
  final bool isLast;
  const PostWidget({Key key, this.post, this.isLast = false}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
              child: Stack(
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Image.network(post.imageUrl,
                        height: 200.0,
                        width: double.infinity,
                        fit: BoxFit.fitWidth,
                        errorBuilder: (context, error, stackTrace) =>
                            ErrorLoadingImageWidget()),
                  ),
                  Positioned(
                    right: 0.0,
                    bottom: 0.0,
                    child: Container(
                      margin: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: AppColors.kTransColor,
                          borderRadius: BorderRadius.circular(15)),
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: Center(child: Text(Util.formatdate(post.date))),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(left: 0.0, top: 5, bottom: 5),
                child: Text('${post.title}',
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.w500))),
            Padding(
                padding: const EdgeInsets.only(left: 0.0, top: 5, bottom: 5),
                child: Text('${post.body}',
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey,
                        fontWeight: FontWeight.w500))),
            Container(
              height: 1.0,
              margin: EdgeInsets.symmetric(vertical: 20),
              width: double.infinity,
              color: isLast ? null : AppColors.kPrimaryColor,
            ),
          ],
        ));
  }
}
