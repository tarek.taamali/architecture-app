import 'package:get/get.dart';
import 'package:inetum/app/data/model/post.dart';
import 'package:inetum/app/data/model/user.dart';
import 'package:inetum/app/data/services/post_repository.dart';
import 'package:inetum/app/modules/user_details/controllers/pagination_filter.dart';
import 'package:inetum/app/modules/list_users/controllers/list_user_controller.dart';

class UserDetailsController extends GetxController {
  final PostRepository postRepository;
  final _posts = <Post>[].obs;
  final _paginationFilter = PaginationFilter().obs;
  final _lastPage = false.obs;

  UserDetailsController(this.postRepository);

  List<Post> get posts => _posts.toList();
  int get limit => _paginationFilter.value.limit;
  int get _page => _paginationFilter.value.page;
  bool get lastPage => _lastPage.value;
  User userDetail = Get.find<ListUsersController>().user;

  @override
  onInit() {
    ever(_paginationFilter, (_) => _getAllUsers());
    _changePaginationFilter(1, 15);
    super.onInit();
  }

  final _user = User().obs;
  get user => this._user.value;
  set user(value) => this._user.value = value;

  Future<void> _getAllUsers() async {
    final postsData = await postRepository.getPostsByUser(userDetail.id,
        _paginationFilter.value.page, _paginationFilter.value.limit);
    if (postsData.isEmpty) {
      _lastPage.value = true;
    }
    _posts.addAll(postsData);
  }

  void changeTotalPerPage(int limitValue) {
    _posts.clear();
    _lastPage.value = false;
    _changePaginationFilter(1, limitValue);
  }

  void _changePaginationFilter(int page, int limit) {
    _paginationFilter.update((val) {
      val.page = page;
      val.limit = limit;
    });
  }

  void loadNextPage() => _changePaginationFilter(_page + 1, limit);
}
