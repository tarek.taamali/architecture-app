import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:inetum/app/data/provider/api.dart';
import 'package:inetum/app/data/services/user_repository.dart';
import 'package:inetum/app/modules/list_users/controllers/list_user_controller.dart';

class ListUsersBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ListUsersController>(() {
      return ListUsersController(
          userRepository:
              UserRepository(apiClient: ApiClient(httpClient: http.Client())));
    });
  }
}
