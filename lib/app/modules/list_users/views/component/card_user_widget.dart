import 'package:flutter/material.dart';
import 'package:inetum/app/common_widget/circle_avatar_widget.dart';
import 'package:inetum/app/data/model/user.dart';

class CardUserWidget extends StatelessWidget {
  const CardUserWidget({Key key, @required this.user, this.onTap})
      : super(key: key);
  final User user;
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTap,
      leading: CircleAvatarWidget(
        avatarUrl: user.avatarUrl,
      ),
      title: Text(
        user.name,
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
      ),
      subtitle: Text(
        user.email,
      ),
    );
  }
}
