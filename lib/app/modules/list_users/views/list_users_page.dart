import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:inetum/app/common_widget/loading_widget.dart';
import 'package:inetum/app/modules/list_users/controllers/list_user_controller.dart';
import 'component/card_user_widget.dart';

class ListUsersPage extends GetView<ListUsersController> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: GetX<ListUsersController>(initState: (state) {
            Get.find<ListUsersController>().getAllUsers();
          }, builder: (_) {
            return _.userList.length < 1
                ? Center(child: LoadingWidget())
                : ListView.separated(
                    separatorBuilder: (BuildContext context, int index) =>
                        const Divider(),
                    itemBuilder: (context, index) {
                      return Column(children: <Widget>[
                        CardUserWidget(
                          user: _.userList[index],
                          onTap: () => _.details(_.userList[index]),
                        ),
                      ]);
                    },
                    itemCount: _.userList.length,
                  );
          }),
        ),
      ),
    );
  }
}
