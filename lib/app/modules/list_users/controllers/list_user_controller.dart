import 'package:inetum/app/data/services/user_repository.dart';
import 'package:inetum/app/routes/app_pages.dart';

import '../../../data/model/user.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

class ListUsersController extends GetxController {
  ListUsersController({@required this.userRepository})
      : assert(userRepository != null);

  final UserRepository userRepository;

  final _usersList = <User>[].obs;
  get userList => this._usersList.value;
  set userList(value) => this._usersList.value = value;

  final _user = User().obs;
  get user => this._user.value;
  set user(value) => this._user.value = value;

  getAllUsers() {
    userRepository.getAllUsers().then((data) {
      this.userList = data;
    });
  }

  details(user) {
    this.user = user;
    Get.toNamed(Routes.DETAILS);
  }
}
