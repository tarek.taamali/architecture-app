import 'package:intl/intl.dart';

class Util {
  const Util._();

  /// format Date to String yyyy-mm-dd
  static String formatdate(DateTime dateTime) {
    final DateFormat format = DateFormat('yyyy-MM-dd');
    return format.format(dateTime);
  }
}
