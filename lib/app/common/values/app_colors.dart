import 'package:flutter/material.dart';

class AppColors {
  const AppColors._();

  static const Color kPrimaryColor = Colors.deepOrange;
  static const Color kAccentColor = Colors.white;
  static const Color kBlackColor = Colors.black;
  static const Color kWhiteColor = Colors.white;
  static Color kTransColor = Colors.white.withOpacity(0.5);
}
