import 'package:flutter/material.dart';
import 'package:inetum/app/common/values/app_colors.dart';

class AppTheme {
  const AppTheme._();

  static ThemeData get theme {
    return ThemeData(
      brightness: Brightness.light,
      primaryColor: AppColors.kPrimaryColor,
      accentColor: AppColors.kAccentColor,
      buttonColor: AppColors.kPrimaryColor,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      appBarTheme: const AppBarTheme(
          color: AppColors.kWhiteColor,
          iconTheme: IconThemeData(color: AppColors.kBlackColor)),
      iconTheme: IconThemeData(color: AppColors.kBlackColor),
    );
  }
}
