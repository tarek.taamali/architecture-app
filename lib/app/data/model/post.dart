class Post {
  String id;
  String title;
  String body;
  String imageUrl;
  DateTime date;

  Post({id, title, body, date});

  Post.fromJson(Map<String, dynamic> json) {
    this.id = json['id'].toString();
    this.title = json['title'];
    this.body = json['body'];
    this.date = DateTime.parse(json["date"]);
    this.imageUrl = json['imageUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.title;
    data['title'] = this.body;
    data['body'] = this.imageUrl;
    data['date'] = this.date;
    return data;
  }
}
