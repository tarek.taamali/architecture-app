class User {
  int id;
  String name;
  String userName;
  String email;
  String avatarUrl;

  User({id, title, body});

  User.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.userName = json['userName'];
    this.email = json['email'];
    this.avatarUrl = json['avatarUrl'];
  }

  
}
