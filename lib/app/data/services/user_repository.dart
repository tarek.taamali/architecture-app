import 'package:inetum/app/data/provider/api.dart';
import 'package:meta/meta.dart';

class UserRepository {
  final ApiClient apiClient;

  UserRepository({@required this.apiClient}) : assert(apiClient != null);

  getAllUsers() {
    return apiClient.getAllUsers();
  }
}
