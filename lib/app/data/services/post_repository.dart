import 'package:inetum/app/data/provider/api.dart';
import 'package:meta/meta.dart';

class PostRepository {
  final ApiClient apiClient;

  PostRepository({@required this.apiClient}) : assert(apiClient != null);

  getPostsByUser(id, page, limit) {
    return apiClient.getPostsByUsers(id, page, limit);
  }
}
