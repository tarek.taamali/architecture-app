import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:inetum/app/common/constants.dart';
import 'package:inetum/app/data/model/post.dart';
import 'package:inetum/app/data/model/user.dart';
import 'package:meta/meta.dart';

class ApiClient {
  final http.Client httpClient;
  ApiClient({@required this.httpClient});

  getAllUsers() async {
    try {
      var response = await httpClient.get(Constants.BASE_URL + "authors");
      if (response.statusCode == 200) {
        Iterable jsonResponse = json.decode(response.body);
        List<User> listUsers =
            jsonResponse.map((model) => User.fromJson(model)).toList();
        return listUsers;
      } else
        print('error');
    } catch (_) {}
  }

  getPostsByUsers(userId, page, limit) async {
    try {
      var response = await httpClient.get(Constants.BASE_URL +
          "posts?authorId=$userId&_page=$page&_limit=$limit");
      if (response.statusCode == 200) {
        Iterable jsonResponse = json.decode(response.body);
        List<Post> listPost =
            jsonResponse.map((post) => Post.fromJson(post)).toList();
        return listPost;
      } else
        print('error');
    } catch (_) {}
  }
}
