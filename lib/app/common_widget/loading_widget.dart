import 'package:flutter/material.dart';
import 'package:inetum/app/common/values/app_colors.dart';
import 'package:loading_indicator/loading_indicator.dart';

class LoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: LoadingIndicator(
        color: AppColors.kPrimaryColor,
        indicatorType: Indicator.ballRotateChase,
      ),
    );
  }
}
