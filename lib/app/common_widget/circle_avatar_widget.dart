import 'package:flutter/material.dart';

class CircleAvatarWidget extends StatelessWidget {
  final String avatarUrl;
  final double width;
  final double height;
  const CircleAvatarWidget(
      {Key key, @required this.avatarUrl, this.width = 50, this.height = 50})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: Colors.white,
          width: 3,
        ),
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(.3),
              offset: Offset(0, 5),
              blurRadius: 25)
        ],
      ),
      child: CircleAvatar(
        backgroundImage: NetworkImage(avatarUrl),
      ),
    );
  }
}
