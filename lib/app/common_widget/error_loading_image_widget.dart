import 'package:flutter/material.dart';

class ErrorLoadingImageWidget extends StatelessWidget {
  final double height;

  const ErrorLoadingImageWidget({Key key, this.height = 200}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      child: Center(child: Icon(Icons.photo, size: 30)),
    );
  }
}
