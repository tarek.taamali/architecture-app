import 'package:get/get.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';
import 'package:inetum/app/modules/list_users/bindings/list_users_binding.dart';
import 'package:inetum/app/modules/user_details/bindings/user_details_binding.dart';
import 'package:inetum/app/modules/user_details/views/user_details_page.dart';
import '../modules/list_users/views/list_users_page.dart';
part './app_routes.dart';

class AppPages {
  static final pages = [
    GetPage(
        name: Routes.INITIAL,
        page: () => ListUsersPage(),
        binding: ListUsersBinding()),
    GetPage(
      name: Routes.DETAILS,
      page: () => UserDetailsPage(),
      binding: UserDetailsBinding(),
      transition: Transition.rightToLeft,
    ),
  ];
}
